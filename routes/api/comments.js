const express = require("express");
const router = express.Router();
const validateComment = require("../../validation/comment");
const isEmpty = require("../../validation/is-empty");
const admin = require("firebase-admin");

//* admin has access to read and write all data
var db = admin.database();

//* List all comments of an article
//* route: GET api/comments/:article_id
router.get("/:article_id", (req, res) => {
  var ref = db.ref();
  var comArray = [];

  ref
    .child("comments")
    .orderByChild("article_id")
    .equalTo(req.params.article_id)
    .on("value", function(snapshot) {
      //*insert into array
      snapshot.forEach(function(comment) {
        comArray.push({ [comment.key]: comment });
      });
      if (isEmpty(comArray)) {
        res.status(400).json({ msg: "no comment" });
      } else {
        comArray.sort(); //*sort the array
        res.json(comArray);
      }
    });
});

//* Comment on an article / comment on a comment (use parent_id)
//* route: POST api/comments
router.post("/", (req, res) => {
  const { errors, isValid } = validateComment(req.body);

  //* Check Validation
  if (!isValid) {
    //* If any errors, send 400 with errors object
    return res.status(400).json(errors);
  }

  let parent = !isEmpty(req.body.parent_id) ? req.body.parent_id : 0; //* parent_id = 0 means root comment

  const comment = {
    nickname: req.body.nickname,
    content: req.body.content,
    creation_date: req.body.creation_date,
    article_id: req.body.article_id,
    parent_id: parent
  };

  //* Validation that need http request
  //* Check if article exists
  var articRef = db.ref("articles").child(req.body.article_id);
  var parentRef = db.ref("comments").child(parent);

  articRef.once("value", function(snapshot) {
    if (!snapshot.hasChild("content")) {
      res.status(400).json({ msg: "Article does not exist" });
    } else {
      //* if article exists, check if parent comment exists
      parentRef.once("value", function(pSnapshot) {
        if (parent != 0 && !pSnapshot.hasChild("content")) {
          res.status(400).json({ msg: "Parent comment does not exist" });
        } else {
          //* Create a new ref
          var ref = db.ref("comments");
          var newItem = ref.push();

          //* Save new data
          newItem.set(comment);

          //* Display the saved data
          ref.child(newItem.key).once("value", function(snapshot) {
            res.json({ [newItem.key]: snapshot });
          });
        }
      });
    }
  });
});

module.exports = router;
