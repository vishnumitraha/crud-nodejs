const express = require("express");
const router = express.Router();
const validateArticle = require("../../validation/article"); // Validate Article Function
const admin = require("firebase-admin");

//* admin has access to read and write all data
var db = admin.database();
var ref = db.ref("articles");
//* Get all articles (20 articles per page)
//* route: GET api/articles/page/:page
router.get("/page/:page", (req, res) => {
  var articArray = [];

  ref.once("value", function(snapshot) {
    //*insert into array
    snapshot.forEach(function(article) {
      articArray.push({ [article.key]: article });
    });

    articArray.sort(); //*sort the array

    //*get data based on page number
    if (articArray.length <= (req.params.page - 1) * 20) {
      res.status(400).json({ msg: "no data" });
    } else if (articArray.length / req.params.page < 20) {
      res.json(articArray.slice((req.params.page - 1) * 20, articArray.length));
    } else {
      res.json(
        articArray.slice((req.params.page - 1) * 20, req.params.page * 20)
      );
    }
  });
});

//* Get an article content (by push id)
//* route: GET api/articles/:id
//* sample: http://localhost:5000/api/articles/-LWpwpjkeNyA-2eQOk_k2
router.get("/:id", (req, res) => {
  ref.child(req.params.id).once("value", function(snapshot) {
    res.json(snapshot.child("content"));
  });
});

//* Post a new article
//* route: POST api/articles
router.post("/", (req, res) => {
  const { errors, isValid } = validateArticle(req.body);

  //* Validation for article
  if (!isValid) {
    //* If any errors, send 400 with errors object
    return res.status(400).json(errors);
  }

  const article = {
    title: req.body.title,
    nickname: req.body.nickname,
    content: req.body.content,
    creation_date: req.body.creation_date
  };

  //* Create a new push key
  var newItem = ref.push();

  //* Save new data
  newItem.set(article);

  //* Display the saved data
  ref.child(newItem.key).once("value", function(snapshot) {
    res.json({ [newItem.key]: snapshot });
  });
});
module.exports = router;
