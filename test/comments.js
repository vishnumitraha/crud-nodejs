//Require the dev-dependencies
let chai = require("chai");
let chaiHttp = require("chai-http");
let expect = chai.expect;

chai.use(chaiHttp);

//Test to get all comments in an article
describe("/GET comments", () => {
  it("it should GET all comments in an article", done => {
    chai
      .request("http://localhost:5000")
      .get("/api/comments/-LWotEmUuJl2ps0-WvsP")
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });
});

//Test to POST a Comment without nickname
describe("/POST a comment", () => {
  it("it should not POST a comment without a nickname", done => {
    let comment = {
      content: "Nice Info",
      creation_date: new Date().getTime(),
      article_id: "-LWp2XKU2WraztH-kKO3",
      parent_id: "-LWsNtMrFsDCQo4D_iEg"
    };
    chai
      .request("http://localhost:5000")
      .post("/api/comments")
      .send(comment)
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res)
          .to.have.property("body")
          .and.has.property("nickname");
        done();
      });
  });
});

//Test to POST a Comment without content
describe("/POST a comment", () => {
  it("it should not POST a comment without a content", done => {
    let comment = {
      nickname: "Levi",
      creation_date: new Date().getTime(),
      article_id: "-LWp2XKU2WraztH-kKO3",
      parent_id: "-LWsNtMrFsDCQo4D_iEg"
    };
    chai
      .request("http://localhost:5000")
      .post("/api/comments")
      .send(comment)
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res)
          .to.have.property("body")
          .and.has.property("content");
        done();
      });
  });
});

//Test to POST a Comment without creation date
describe("/POST a comment", () => {
  it("it should not POST a comment without a creation date", done => {
    let comment = {
      nickname: "Vince",
      content: "hmm",
      article_id: "-LWp2XKU2WraztH-kKO3",
      parent_id: "-LWsNtMrFsDCQo4D_iEg"
    };
    chai
      .request("http://localhost:5000")
      .post("/api/comments")
      .send(comment)
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res)
          .to.have.property("body")
          .and.has.property("creation_date");
        done();
      });
  });
});
