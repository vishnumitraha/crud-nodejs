const express = require("express");
const bodyParser = require("body-parser");
const config = require("./config");
const admin = require("firebase-admin");
const serviceAccount = require("./imaginato-task-firebase.json");

//* Initialize firebase database and credential
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://imaginato-task.firebaseio.com"
});

const app = express();

const articles = require("./routes/api/articles");
const comments = require("./routes/api/comments");

//* Body Parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//* Routes
app.use("/api/articles", articles);
app.use("/api/comments", comments);
app.get("/", (req, res) => {
  res.json({
    "Endpoints are this URL":
      "https://documenter.getpostman.com/view/3664570/RztfwXcZ"
  });
});
app.listen(config.port, () =>
  console.log(`Server running on port ${config.port}`)
);
