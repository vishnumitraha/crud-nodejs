module.exports = {
  env: "development",
  port: 5000,
  jwtSecret: "my-lil-secret",
  jwtDuration: "2 hours"
};
