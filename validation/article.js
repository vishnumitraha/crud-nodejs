const isEmpty = require("./is-empty");
module.exports = function validateArticle(data) {
  let errors = {};

  if (isEmpty(data.title)) {
    errors.title = "Title is required";
  }
  if (isEmpty(data.nickname)) {
    errors.nickname = "Nickname is required";
  }
  if (isEmpty(data.content)) {
    errors.content = "Content is required";
  }
  if (isEmpty(data.creation_date)) {
    errors.creation_date = "Creation date is required";
  }
  return {
    errors: errors,
    isValid: isEmpty(errors)
  };
};
